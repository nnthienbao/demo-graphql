package com.baonnt.demographql.graphql;

import java.util.Arrays;
import java.util.List;

import com.baonnt.demographql.dto.Address;
import com.baonnt.demographql.dto.User;

import org.springframework.stereotype.Component;

import graphql.schema.DataFetcher;

@Component
public class GraphQLDataFetchers {

    private static final List<User> _LIST_USER;
    static {
        final User user1 = new User(0, "Nguyen Van A", 18, new Address("12 Truong Chinh", 700000));
        final User user2 = new User(1, "Nguyen Van B", 18, new Address("24 Le Duan", 700000));
        final User user3 = new User(2, "Nguyen Van C", 18, new Address("255 Tran Binh Trong", 700000));
        final User user4 = new User(3, "Nguyen Van D", 18, new Address("77 Nguyen Tat Thanh", 700000));
        _LIST_USER = Arrays.asList(user1, user2, user3, user4);
    }

	public DataFetcher getListUser() {
		return enviroment -> {
			return _LIST_USER;
		};
	}

	public DataFetcher getUser() {
		return enviroment -> {
            int id = enviroment.getArgument("id");
            return _LIST_USER.stream().filter(user -> user.getId()==id).findFirst().get();
		};
	}
    
}