package com.baonnt.demographql.controllers;

import com.baonnt.demographql.dto.Hello;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    
	@GetMapping(path = "/hello")
    public Hello getHello() {
        Hello hello = new Hello();
        hello.setMessage("Hello");
        return hello;
    }
}