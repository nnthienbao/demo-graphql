package com.baonnt.demographql.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Address {
    private String name;
    private int zipCode;
}